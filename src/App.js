import React, { useEffect, useState } from "react";
import './App.css';
import './styles.css';

function App() {
const [data, setData] = useState(null);
  const [totalResult, setTotalResult] = useState(0);
 const apiUrl = "https://spe-academy.spesolution.com/api/recruitment";

 useEffect(() => {
   const fetchData = async () => {
     try {
       const response = await fetch(apiUrl, {
         method: "GET",
         headers: {
           "Content-Type": "application/json",
           Authorization: "Bearer o7Ytbt9XQLI3PgtebJfKSXKEf0XHU74Y",
         },
       });

       if (!response.ok) {
         throw new Error("Network response was not ok");
       }

       const result = await response.json();
      const res = result?.reduce(
        (total, item) => total + item.product.price * item.quantity,
        0
      );
      setTotalResult(res);

       setData(result);
     } catch (error) {
       console.error("Error fetching data:", error);
     }
   };
   console.log("datapa", data);
   fetchData();

 }, []);

 
  const formatToIDR = (value) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
    }).format(value);
  };
  return (
    <div>
      <div className="container">
        <h1>React Component</h1>
      </div>
      <h2>SPE Frontend Shop</h2>
      <div className="container-list">
        <div className="container-table">
          <div className="header">
            <thead>
              <tr>
                <div style={{ display: "flex" }}>
                  <p className="product">Product</p>
                  <p className="qty">Quantity</p>
                  {/* <th></th> */}
                  <p className="subtotal">Subtotal</p>
                </div>
              </tr>
            </thead>
          </div>
          <div className="body">
            <tbody className="bodytable">
              {data ? (
                data?.map((item, index) => (
                  <>
                    <tr>
                      <td>
                        <div className="image-description-container">
                          <div>
                            <img
                              src={item?.product?.media_url}
                              alt="Deskripsi Gambar"
                            />
                          </div>
                          <div className="description-container">
                            <h4>{item?.product?.code}</h4>
                            <p>{item?.product?.name}</p>
                            <p className="priceImage">
                              IDR {formatToIDR(item?.product?.price)}
                            </p>

                            <p className="stockImage">
                              {item?.product?.stock} in Stock
                            </p>
                          </div>
                        </div>
                      </td>
                      <td >{item?.quantity}</td>
                      {/* <td></td> */}
                      <td className="totalitem">
                        {" "}
                        IDR {formatToIDR(item.product.price * item.quantity)}
                      </td>
                    </tr>
                    <hr />
                  </>
                ))
              ) : (
                <p>Loading...</p>
              )}
              {/* Add more rows as needed */}
            </tbody>
              <div className="footer">
                <div style={{ display: "flex" }}>
                  <p style={{ marginLeft: "700px" }}>Total </p>
                  <p style={{ marginLeft: "90px" }}>
                    {" "}
                    IDR {formatToIDR(totalResult)}
                  </p>
                </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
